<?php
namespace testtask\components;

use Bitrix\Catalog\GroupTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main;
use Bitrix\Main\Config\Option;
use CBitrixComponent;
use CIBlock;
use CIBlockElement;
use Exception;

/**
 * Class RegionDetect
 *
 * @package testtask\components
 */
class RegionDetect extends CBitrixComponent
{
    /** @var array */
    private $iblock;

    /** @var string */
    private $basePricetypeCode;

    //:::::::::::::::::::::::::::::::::::::::::::::::  Public actions  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @inheritDoc
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['REGIONS_IBLOCK_CODE'] = (string) $arParams['REGIONS_IBLOCK_CODE'];

        return $arParams;
    } // -END- public function onPrepareComponentParams()

    /** ----------------------------------------------------------------------------------------------------------------
     * @inheritDoc
     *
     * @throws Exception
     */
    public function executeComponent()
    {
        Main\Loader::includeModule('iblock');

        $basePricetype = GroupTable::getList(['filter' => ['BASE'=>'Y']])->fetch();
        $this->basePricetypeCode = $basePricetype['NAME'] ?? 'BASE';

        $this->iblock = CIBlock::getList([], [
            'CODE' => $this->arParams['REGIONS_IBLOCK_CODE'],
            'CHECK_PERMISSIONS' => 'N']
        )->Fetch();

        $subdomain = $this->extractSubdomain();

        $_SESSION['REGIONALITY'] = $this->iblock ? $this->getBySubdomain($subdomain) : $this->getUnefinedRegion();

        $this->includeComponentTemplate();
    } // -END- public function executeComponent()

    //::::::::::::::::::::::::::::::::::::::::::::::  Private helpers  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @param string $subdomain
     *
     * @return array
     */
    private function getBySubdomain($subdomain)
    {
        $regionFields = $this->getFields($subdomain) ?: $this->getFields($this->basePricetypeCode);

        return $regionFields ? $this->composeRegion($regionFields) : $this->getUnefinedRegion();
    } // -END- private function getBySubdomain()

    /** ----------------------------------------------------------------------------------------------------------------
     * @param string $code
     *
     * @return array|false
     */
    private function getFields($code)
    {
        $select = ['ID', 'NAME', 'CODE'];
        $filter = ['IBLOCK_ID'=>$this->iblock['ID'], '=CODE'=>$code];

        try {
            return ElementTable::getList(['select' => $select, 'filter' => $filter])->fetch();
        }
        catch (Main\SystemException $e) {
            return false;
        }
    } // -END- private function getFields()

    /** ----------------------------------------------------------------------------------------------------------------
     * @param array $fields
     *
     * @return array
     */
    private function composeRegion($fields)
    {
        $res = $fields;

        $prop = CIBlockElement::getProperty($this->iblock['ID'], $fields['ID'], [], ['CODE'=>'PHONE'])->Fetch();

        $res['PHONE'] = $prop ? (string) $prop['VALUE'] : '';

        return $res;
    } // -END- private function composeRegion()

    /** ----------------------------------------------------------------------------------------------------------------
     * @return string
     *
     * @throws Main\SystemException
     */
    private function extractSubdomain()
    {
        $context = Main\Application::getInstance()->getContext();

        if (!($domain = Option::get('main', 'server_name'))) {
            $server = $context->getServer();
            $domain = defined('SITE_SERVER_NAME') && SITE_SERVER_NAME ? SITE_SERVER_NAME : $server->getServerName();
        }

        $domainLen = substr_count($domain, '.') + 1;

        return implode('.', array_slice(explode('.', $context->getRequest()->getHttpHost()), 0, -$domainLen));
    } // -END- private function extractSubdomain()

    /** ----------------------------------------------------------------------------------------------------------------
     * @return array
     */
    private function getUnefinedRegion()
    {
        return [
            'ID'    => 0,
            'NAME'  => 'Город не определен',
            'CODE'  => $this->basePricetypeCode,
            'PHONE' => '',
        ];
    } // -END- private function getUnefinedRegion()
} // -END- class RegionDetect
