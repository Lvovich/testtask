<?php if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)exit();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="header-city">
    <a href="javascript:void(0)" data-src="#city-popup" data-fancybox="" class="header-city-inner">
        <span class="header-city-ico"></span>
        <span class="header-city-text"><?= $_SESSION['REGIONALITY']['NAME'] ?></span>
    </a>
</div>
