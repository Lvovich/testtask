<?php if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)exit();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$regionsList = $arResult['REGIONS'];
?>
<div id="city-popup">
    <div class="city-popup">
        <?php if ($regionsList): ?>
            <div class="city-popup__title">
                <span>Выберите ваш город из списка</span>
            </div>
            <div class="city-popup__list">
                <div class="city-popup__list-row">
                    <?php foreach (array_chunk($regionsList, ceil(count($regionsList) / 3), true) as $chunk): ?>
                        <div class="city-popup__list-column">
                            <?php foreach ($chunk as $subdomain => $region): ?>
                                <div class="city-popup__list-item">
                                    <?php if ($region['CURRENT']): ?>
                                        <span><?= $region['NAME'] ?></span>
                                    <?php else: ?>
                                        <a href="<?= $region['REDIRECT_URL'] ?>"><?= $region['NAME'] ?></a>
                                    <?php endif ?>
                                </div>
                            <?php endforeach ?>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>

        <?php else: ?>
            <div class="city-popup__title">
                <span>Нет регионов, доступных для выбора</span>
            </div>

        <?php endif ?>
    </div>
</div>
