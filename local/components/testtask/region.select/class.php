<?php
namespace testtask\components;

use Bitrix\Catalog\GroupTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main;
use Bitrix\Main\Config\Option;
use CBitrixComponent;
use CIBlock;
use Exception;

/**
 * Class RegionSelect
 *
 * @package testtask\components
 */
class RegionSelect extends CBitrixComponent
{
    /** @var array */
    private $iblock;

    /** @var string */
    private $basePricetypeCode;

    //:::::::::::::::::::::::::::::::::::::::::::::::  Public actions  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @inheritDoc
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['REGIONS_IBLOCK_CODE'] = (string) $arParams['REGIONS_IBLOCK_CODE'];

        return $arParams;
    } // -END- public function onPrepareComponentParams()

    /** ----------------------------------------------------------------------------------------------------------------
     * @inheritDoc
     *
     * @throws Exception
     */
    public function executeComponent()
    {
        Main\Loader::includeModule('iblock');

        $basePricetype = GroupTable::getList(['filter' => ['BASE'=>'Y']])->fetch();
        $this->basePricetypeCode = $basePricetype['NAME'] ?? 'BASE';

        $this->iblock = CIBlock::getList([], [
            'CODE' => $this->arParams['REGIONS_IBLOCK_CODE'],
            'CHECK_PERMISSIONS' => 'N']
        )->Fetch();

        $this->arResult['REGIONS'] = $this->iblock ? $this->getRegionsList() : [];

        $this->includeComponentTemplate();
    } // -END- public function executeComponent()

    //::::::::::::::::::::::::::::::::::::::::::::::  Private helpers  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @return array
     */
    private function getRegionsList()
    {
        $res = [];

        $select = ['ID', 'NAME', 'CODE'];
        $filter = ['IBLOCK_ID'=>$this->iblock['ID'], 'ACTIVE'=>'Y'];

        try {
            $server = Main\Application::getInstance()->getContext()->getServer();

            if (!($domain = Option::get('main', 'server_name'))) {
                $domain = defined('SITE_SERVER_NAME') && SITE_SERVER_NAME ? SITE_SERVER_NAME : $server->getServerName();
            }

            $dbRes = ElementTable::getList(['select' => $select, 'filter' => $filter]);
        }
        catch (Main\SystemException $e) {
            return [];
        }

        while ($region = $dbRes->fetch()) {
            $subdomain = ($region['CODE'] === $this->basePricetypeCode) ? '' : "{$region['CODE']}.";

            $res[$region['CODE']] = [
                'NAME'    => $region['NAME'],
                'CURRENT' => $region['CODE'] === $_SESSION['REGIONALITY']['CODE'],
                'REDIRECT_URL' => "//{$subdomain}{$domain}{$server->getRequestUri()}",
            ];
        }

        return $res;
    } // -END- private function getRegionsList()
} // -END- class RegionSelect
