<?php
namespace testtask\lib;

use Bitrix\Catalog\PriceTable;
use Bitrix\Catalog\Product\CatalogProvider;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\SystemException;
use Bitrix\Sale\Result;
use CCatalogGroup;

try {
    Loader::includeModule('catalog');
}
catch (LoaderException $e) {
    return;
}

/**
 * Class RegionalityCatalogProvider
 *
 * @package testtask\lib
 */
class RegionalityCatalogProvider extends CatalogProvider
{
    //:::::::::::::::::::::::::::::::::::::::::::::::  Public actions  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @inheritDoc
     */
    public function getProductData(array $products)
    {
        /** @var Result $res */
        $res = parent::getProductData($products);

        $originalData = $res->getData();

        $hasChanges = false;

        if ($regionalPriceType = CCatalogGroup::GetList([], ['NAME'=>$_SESSION['REGIONALITY']['CODE']])->Fetch()) {
            foreach ($products as $pid => $pData) {
                if (isset($originalData['PRODUCT_DATA_LIST'][$pid])) {
                    $priceList = &$originalData['PRODUCT_DATA_LIST'][$pid]['PRICE_LIST'][$pData['BASKET_ID']];

                    try {
                        $regionalPrice = PriceTable::getList(['filter' =>
                            ['PRODUCT_ID' => $pid, 'CATALOG_GROUP_ID' => $regionalPriceType['ID']]
                        ])->fetch();

                        if ($regionalPrice && $priceList['PRICE_TYPE_ID'] !== $regionalPriceType['ID']) {
                            $priceList['PRODUCT_PRICE_ID'] = $regionalPrice['ID'];
                            $priceList['NOTES'] = $regionalPriceType['NAME_LANG'];
                            $priceList['PRICE_TYPE_ID'] = $regionalPriceType['ID'];
                            $priceList['BASE_PRICE'] = $regionalPrice['PRICE'];

                            $hasChanges = true;
                        }
                    }
                    catch (SystemException $e) {
                    }
                }
            }
        }

        if ($hasChanges) {
            $res->setData($originalData);
        }

        return $res;
    } // -END- public function getProductData()

    //::::::::::::::::::::::::::::::::::::::::::::::  Private helpers  ::::::::::::::::::::::::::::::::::::::::::::::://

} // -END- class RegionalityCatalogProvider
