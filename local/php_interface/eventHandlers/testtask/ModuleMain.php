<?php
namespace testtask\eventHandlers;

/**
 * Class ModuleMain
 *
 * @package testtask\eventHandlers
 */
class ModuleMain
{
    //:::::::::::::::::::::::::::::::::::::::::::::::  Public actions  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @param string $content
     */
    public static function replaceSEOVariables(&$content)
    {
        if (!empty($_SESSION['REGIONALITY'])) {
            $content = str_replace('{#regionality.NAME#}', $_SESSION['REGIONALITY']['NAME'], $content);
        }
    } // -END- static function replaceSEOVariables()
} // -END- class ModuleMain
