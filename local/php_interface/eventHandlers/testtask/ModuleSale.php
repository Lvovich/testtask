<?php
namespace testtask\eventHandlers;

use Bitrix\Main\Event;
use Bitrix\Sale\BasketItem;
use Exception;

/**
 * Class ModuleSale
 *
 * @package testtask\eventHandlers
 */
class ModuleSale
{
    //:::::::::::::::::::::::::::::::::::::::::::::::  Public actions  ::::::::::::::::::::::::::::::::::::::::::::::://
    /**
     * @param Event $event
     */
    public static function replaceProductProvider(Event $event)
    {
        /** @var BasketItem $basket */
        if ($item = $event->getParameter('ENTITY')) {
            try {
                $className = '\testtask\lib\RegionalityCatalogProvider';

                if (class_exists($className)) {
                    $item->setField('PRODUCT_PROVIDER_CLASS', $className);
                }
            }
            catch (Exception $e) {
            }
        }
    } // -END- static function replaceProductProvider()
} // -END- class ModuleSale
