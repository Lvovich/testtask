<?php
@include_once __DIR__ . '/eventHandlers/testtask/ModuleSale.php';
@include_once __DIR__ . '/eventHandlers/testtask/ModuleMain.php';
@include_once __DIR__ . '/eventHandlers/testtask/lib/RegionalityCatalogProvider.php';

use Bitrix\Main\EventManager;

$manager = EventManager::getInstance();

$manager->addEventHandler('sale', 'OnSaleBasketItemRefreshData',
    ['testtask\eventHandlers\ModuleSale', 'replaceProductProvider']
);

$manager->addEventHandler('main', 'OnEndBufferContent',
    ['testtask\eventHandlers\ModuleMain', 'replaceSEOVariables']
);
